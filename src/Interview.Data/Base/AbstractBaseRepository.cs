﻿using System.Threading.Tasks;
using Interview.Data.Interface.Base;
using Microsoft.EntityFrameworkCore;

namespace Interview.Data.Base
{
    public class AbstractBaseRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        public AbstractBaseRepository(DbContext context)
        {
            _context = context;
        }

        public T Create(T element)
        {
            var entry = _context.Entry(element);
            entry.State = EntityState.Added;
            _context.Set<T>().Add(element);
            return element;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}

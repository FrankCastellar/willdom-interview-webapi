﻿using Interview.Data.Base;
using Interview.Data.Interface.Base;
using Interview.Data.Interface.Models;
using Interview.Data.Interface.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Interview.Data.Repositories
{
    public class LogRepository : AbstractBaseRepository<Log>, ILogRepository
    {
        public LogRepository(DbContext context) : base(context)
        {
        }
    }
}

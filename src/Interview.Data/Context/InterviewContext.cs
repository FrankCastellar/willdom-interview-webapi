﻿using Interview.Data.Interface.Models;
using Microsoft.EntityFrameworkCore;

namespace Interview.Data.Context
{
    public class InterviewContext : DbContext
    {
        public InterviewContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Log> Logs { get; set; }
    }
}

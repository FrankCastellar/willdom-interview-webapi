﻿using System;
using System.Threading.Tasks;
using Interview.Business.Interface.Logic;
using Interview.Business.Interface.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Interview.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ExamController : Controller
    {
        private readonly ILogLogic _logLogic;
        private readonly IExamLogic _examLogic;

        public ExamController(ILogLogic logLogic, IExamLogic examLogic)
        {
            _logLogic = logLogic;
            _examLogic = examLogic;
        }

        [HttpGet]
        [Route("{number}")]
        public async Task<IActionResult> Execute(int number)
        {
            await _logLogic.CreateLogAsync(number, LogTypeModel.Info, $"Executing exam process for the number: [{number}]");
            try
            {
                var response = _examLogic.Execute(number);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var error = $"Error at executing process for number: [{number}], error -> {ex}";
                await _logLogic.CreateLogAsync(number, LogTypeModel.Error, error);
                return StatusCode(StatusCodes.Status500InternalServerError, error);
            }
        }
    }
}

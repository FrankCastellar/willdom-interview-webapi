﻿using Interview.Business.Interface.Logic;
using Interview.Business.Logic;
using Interview.Business.Mapper;
using Interview.Data.Context;
using Interview.Data.Interface.Repositories;
using Interview.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Interview.WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddApiDependencies(this IServiceCollection services, IConfiguration config)
        {
            // General.
            services.AddHealthChecks();
            services.AddAutoMapper(typeof(InterviewProfileMapper));

            // Data Access Layer.
            services.AddDbContext<DbContext, InterviewContext>(opt => opt.UseSqlServer(config.GetConnectionString("InterviewDB")));
            services.AddTransient<ILogRepository, LogRepository>();

            // Business Logic Layer.
            services.AddTransient<ILogLogic, LogLogic>();
            services.AddTransient<IExamLogic, ExamLogic>();

            return services;
        }
    }
}

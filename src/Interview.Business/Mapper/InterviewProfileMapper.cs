﻿using AutoMapper;
using Interview.Business.Interface.Models;
using Interview.Data.Interface.Models;

namespace Interview.Business.Mapper
{
    public class InterviewProfileMapper : Profile
    {
        public InterviewProfileMapper()
        {
            CreateMap<Log, LogModel>()
                .ReverseMap();
        }
    }
}

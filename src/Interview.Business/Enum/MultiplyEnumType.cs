﻿using System;

namespace Interview.Business.Enum
{
    [Flags]
    public enum MultiplyEnumType
    {
        None = 1 << 0, // 
        Fizz = 1 << 1, // 3
        Buzz = 1 << 2, // 5
        Jazz = 1 << 3 // 7
    }
}

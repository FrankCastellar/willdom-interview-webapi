﻿using System.Threading.Tasks;
using AutoMapper;
using Interview.Business.Interface.Logic;
using Interview.Business.Interface.Models;
using Interview.Data.Interface.Models;
using Interview.Data.Interface.Repositories;

namespace Interview.Business.Logic
{
    public class LogLogic : ILogLogic
    {
        private readonly ILogRepository _logRepository;
        private readonly IMapper _mapper;

        public LogLogic(ILogRepository logRepository, IMapper mapper)
        {
            _logRepository = logRepository;
            _mapper = mapper;
        }

        public async Task<LogModel> CreateLogAsync(int requestNumber, LogTypeModel type, string message)
        {
            var element = new Log
            {
                Message = message,
                LogType = type.ToString()
            };

            var createdElement = _logRepository.Create(element);
            await _logRepository.SaveChangesAsync();
            return _mapper.Map<LogModel>(createdElement);
        }
    }
}

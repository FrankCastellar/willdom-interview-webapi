﻿using System.Collections.Generic;
using System.Text;
using Interview.Business.Enum;
using Interview.Business.Interface.Logic;

namespace Interview.Business.Logic
{
    public class ExamLogic : IExamLogic
    {
        public IList<string> Execute(int requestNumber)
        {
            var results = new List<string>();
            for (var i = 1; i <= requestNumber; i++)
            {
                var number = i;
                var multipliers = DetermineMultipliers(number);
                if (multipliers == MultiplyEnumType.None)
                {
                    results.Add(number.ToString());
                    continue;
                }

                var numberText = new StringBuilder();
                if (multipliers.HasFlag(MultiplyEnumType.Fizz))
                {
                    numberText.Append("Fizz");
                }

                if (multipliers.HasFlag(MultiplyEnumType.Buzz))
                {
                    numberText.Append("Buzz");
                }

                if (multipliers.HasFlag(MultiplyEnumType.Jazz))
                {
                    numberText.Append("Jazz");
                }

                results.Add(numberText.ToString());
            }

            return results;
        }

        private MultiplyEnumType DetermineMultipliers(int number)
        {
            var flags = MultiplyEnumType.None;
            if (number % 3 == 0)
            {
                flags |= MultiplyEnumType.Fizz;
            }

            if (number % 5 == 0)
            {
                flags |= MultiplyEnumType.Buzz;
            }

            if (number % 7 == 0)
            {
                flags |= MultiplyEnumType.Jazz;
            }

            return flags;
        }
    }
}

﻿using System.Threading.Tasks;
using Interview.Business.Interface.Models;

namespace Interview.Business.Interface.Logic
{
    public interface ILogLogic
    {
        Task<LogModel> CreateLogAsync(int requestNumber, LogTypeModel type, string message);
    }
}

﻿using System.Collections.Generic;

namespace Interview.Business.Interface.Logic
{
    public interface IExamLogic
    {
        IList<string> Execute(int requestNumber);
    }
}

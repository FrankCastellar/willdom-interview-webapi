﻿namespace Interview.Business.Interface.Models
{
    public enum LogTypeModel
    {
        Error = 0,
        Info,
        Warning
    }
}

﻿namespace Interview.Business.Interface.Models
{
    public class LogModel
    {
        public int Id { get; set; }
        public LogTypeModel LogType { get; set; }
        public string Message { get; set; }
    }
}

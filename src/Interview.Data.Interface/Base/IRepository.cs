﻿using System.Threading.Tasks;

namespace Interview.Data.Interface.Base
{
    public interface IRepository<T> where T: class
    {
        T Create(T element);
        Task<int> SaveChangesAsync();
    }
}

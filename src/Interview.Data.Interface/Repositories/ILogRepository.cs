﻿using Interview.Data.Interface.Base;
using Interview.Data.Interface.Models;

namespace Interview.Data.Interface.Repositories
{
    public interface ILogRepository : IRepository<Log>
    {
    }
}

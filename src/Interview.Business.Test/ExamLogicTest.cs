using System.Collections.Generic;
using System.Linq;
using Interview.Business.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Interview.Business.Test
{
    [TestClass]
    public class ExamLogicTest
    {
        [TestMethod]
        public void ExecuteSingleFizzTest_ShouldPass()
        {
            // arrange
            var logic = new ExamLogic();

            // execute
            var firstResult = logic.Execute(3).ToList();

            // assert
            CollectionAssert.AreEqual(new List<string> {"1", "2", "Fizz" }, firstResult);
        }

        [TestMethod]
        public void ExecuteSingleBussTest_ShouldPass()
        {
            // arrange
            var logic = new ExamLogic();

            // execute
            var result = logic.Execute(6).ToList();


            // assert
            CollectionAssert.AreEqual(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz" }, result);
        }

        [TestMethod]
        public void ExecuteFizzBuzzCombinedTest_ShouldPass()
        {
            // arrange
            var logic = new ExamLogic();

            // execute
            var result = logic.Execute(15).ToList();

            // assert
            CollectionAssert.AreEqual(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "Jazz","8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "Jazz",
                "FizzBuzz",
            }, result);
        }

        [TestMethod]
        public void ExecuteFizzBuzzJazzCombinedTest_ShouldPass()
        {
            // arrange
            var logic = new ExamLogic();

            // execute
            var result = logic.Execute(105).ToList();

            // assert
            CollectionAssert.AreEqual(new List<string>
            {
                "1",
                "2",
                "Fizz",
                "4",
                "Buzz",
                "Fizz",
                "Jazz",
                "8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "Jazz",
                "FizzBuzz",
                "16",
                "17",
                "Fizz",
                "19",
                "Buzz",
                "FizzJazz",
                "22",
                "23",
                "Fizz",
                "Buzz",
                "26",
                "Fizz",
                "Jazz",
                "29",
                "FizzBuzz",
                "31",
                "32",
                "Fizz",
                "34",
                "BuzzJazz",
                "Fizz",
                "37",
                "38",
                "Fizz",
                "Buzz",
                "41",
                "FizzJazz",
                "43",
                "44",
                "FizzBuzz",
                "46",
                "47",
                "Fizz",
                "Jazz",
                "Buzz",
                "Fizz",
                "52",
                "53",
                "Fizz",
                "Buzz",
                "Jazz",
                "Fizz",
                "58",
                "59",
                "FizzBuzz",
                "61",
                "62",
                "FizzJazz",
                "64",
                "Buzz",
                "Fizz",
                "67",
                "68",
                "Fizz",
                "BuzzJazz",
                "71",
                "Fizz",
                "73",
                "74",
                "FizzBuzz",
                "76",
                "Jazz",
                "Fizz",
                "79",
                "Buzz",
                "Fizz",
                "82",
                "83",
                "FizzJazz",
                "Buzz",
                "86",
                "Fizz",
                "88",
                "89",
                "FizzBuzz",
                "Jazz",
                "92",
                "Fizz",
                "94",
                "Buzz",
                "Fizz",
                "97",
                "Jazz",
                "Fizz",
                "Buzz",
                "101",
                "Fizz",
                "103",
                "104",
                "FizzBuzzJazz"
            }, result);
        }
    }
}
